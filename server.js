//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyparser = require('body-parser');
app.use(bodyparser.json());
app.use(function (req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var requestjson = require('request-json');
var requestjsonpost = require('request-json');

var path = require('path');

var urlLogin = "https://api.mlab.com/api/1/databases/oquesquen/collections/clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlLoginCompl = "&q={'usuarioCliente':'";
var userFront = "";
var urlComma = "',";
var urlLoginCompl2 = "'passCliente':'";
var passFront = "";
var urlLoginCompl3 ="'}&f={'idCliente':1,'nombreCliente':1,'apellidoCliente':1}";

var urlCuentas = "https://api.mlab.com/api/1/databases/oquesquen/collections/cuentas?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlCuentasCompl = "&q={'idCliente':";
var idClienteFront;
var urlCuentasCompl2 ="}";

var urlFiltroCuentasCompl = "&f={'nroCuenta':";

var urlMovsBBVA = "https://api.mlab.com/api/1/databases/oquesquen/collections/movsbbva?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlMovsBBVACompl = "&q={'nroCuenta':'";
var nroCuentaFront;
var urlMovsBBVACompl2 ="'}";

var loginMlab;
var cuentaMlab;
var movbbvaMlab = requestjson.createClient(urlMovsBBVA + urlMovsBBVACompl + nroCuentaFront + urlMovsBBVACompl2);

var movbbvaMlabPost = requestjson.createClient(urlMovsBBVA);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req,res){
  res.sendFile(path.join(__dirname,'index.html'));
})

app.post("/", function(req,res){
  res.send("Hemos recibido su petición post");
})

app.get("/Clientes/:idcliente", function(req,res){
  res.send("Aquí tienes al cliente número : "+ req.params.idcliente);
})

app.put("/", function(req,res){
  res.send("Hemos recibido su petición put cambiada");
})

app.delete("/", function(req,res){
  res.send("Hemos recibido su petición delete");
})

app.get("/v1/movimientos", function(req,res){
  res.sendFile(path.join(__dirname,'movimientosv1.json'));
})

var movimientosJSON = require('./movimientosv2.json');

app.get("/v2/movimientos", function(req,res){
  res.json(movimientosJSON);
})

app.get("/v2/movimientos/:id", function(req,res){
  console.log(req.params.id);
  res.send(movimientosJSON[req.params.id-1]);
})

app.get("/v2/movimientos/:id/:id2", function(req,res){
  console.log(req.params.id);
  console.log(req.params.id2);
  res.send(movimientosJSON[req.params.id-1]);
})

app.get('/v2/movimientosq', function(req,res){
  console.log(req.query);
  res.send("recibido");
})

var bodyparser = require('body-parser');

app.use(bodyparser.json());

app.post('/v2/movimientos', function(req,res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send("movimiento dado de alta");
})

app.get('/movimientos',function(req,res){

    cuentaMlab.get('',function(err, resM, body){
        if(err){
          console.log(err);
        } else {
          res.send(body);
        }
    });
})

app.post('/movimientos', function(req,res){
    clienteMlab.post('',req.body, function(err,resM, body){
      res.send(body);
    });
})

app.get('/login',function(req,res){

    loginMlab.get('',function(err, resM, body){
        if(err){
          console.log(err);
        } else {
          res.send(body);
        }
    });
})

app.get('/v2/login/:usuario/:passw',function(req,res){
  this.userFront = req.params.usuario;
  this.passFront = req.params.passw;
  loginMlab = requestjson.createClient(urlLogin + urlLoginCompl + this.userFront + urlComma + urlLoginCompl2 + this.passFront + urlLoginCompl3);
  loginMlab.get('',function(err, resM, body){
      if(err){
        console.log(err);
      } else {
        res.send(body);
      }
  });

})


app.get('/cuentas',function(req,res){

    cuentaMlab.get('',function(err, resM, body){
        if(err){
          console.log(err);
        } else {
          res.send(body);
        }
    });
})

app.get('/cuentas/:idcliente',function(req,res){
    this.idClienteFront = req.params.idcliente;
    console.log(urlCuentas + urlCuentasCompl + this.idClienteFront + urlCuentasCompl2);
    cuentaMlab = requestjson.createClient(urlCuentas + urlCuentasCompl + this.idClienteFront + urlCuentasCompl2);

    cuentaMlab.get('',function(err, resM, body){
        if(err){
          console.log(err);
        } else {
          res.send(body);
        }
    });
})

app.put('/cuentas', function(req,res){
  console.log("Hemos recibido su petición put");
})

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("X-Custom-Header", "ProcessThisInmediately");
  next();
});

app.put('/cuentas/:nrocuenta/:saldodisponible/:saldocontable', function(req,res){
  req.header("Access-Control-Allow-Methods", "PUT, GET, POST");
  console.log("Hemos recibido su petición put");

  this.cuentaFront = req.params.nrocuenta;

  //Actualiza Cuenta
  var jsonCuentaCompl1="{$set:{";
  var jsonCuenta = "\"saldoDisponible\":";
  this.nuevoSaldoFront = req.params.saldodisponible;
  var urlCommaSinApost = ",";
  var jsonCuentaComp2 = "\"saldoContable\":";
  this.nuevoSaldoContableFront = req.params.saldocontable;
  var urlCommaSinApost = ",";
  var jsonFechaMov = "\"fechaModif\":\"";
  this.fechaMov = (new Date()).toLocaleDateString();
  var jsonCuentaCompl8="\"}}";

  var jsonCuentaFinal = jsonCuentaCompl1 + jsonCuenta + this.nuevoSaldoFront + urlCommaSinApost +
                        jsonCuentaComp2 + this.nuevoSaldoContableFront + urlCommaSinApost +
                        jsonFechaMov + this.fechaMov + jsonCuentaCompl8;

  req.body = JSON.parse(jsonCuentaFinal);
  cuentaMlab = requestjson.createClient(urlCuentas + urlFiltroCuentasCompl + this.cuentaFront + urlCuentasCompl2);

  cuentaMlab.put('',req.body,function(err, resM, body){
    res.send(body);
  });

})

app.get('/movsbbva',function(req,res){

    movbbvaMlab.get('',function(err, resM, body){
        if(err){
          console.log(err);
        } else {
          res.send(body);
        }
    });
})

app.get('/movsbbva/:nrocuenta',function(req,res){

  this.nroCuentaFront = req.params.nrocuenta;
  movbbvaMlab = requestjson.createClient(urlMovsBBVA + urlMovsBBVACompl + this.nroCuentaFront + urlMovsBBVACompl2);
  movbbvaMlab.get('',function(err, resM, body){
      if(err){
        console.log(err);
      } else {
        res.send(body);
      }
  });
})

app.post('/movsbbva/:cuenta/:tipoMov/:importe/:descripcion', function(req,res){

  //Movimiento Cuenta
  var jsonCuentaOrdCompl1="{";
  var jsonCuentaOrd = "\"nroCuenta\":\"";
  this.cuenta = req.params.cuenta;
  var urlComma = "\",";
  var urlCommaSinApost = ",";
  var jsonCuentaOrdComp2 = "\"importe\":";
  this.importe = req.params.importe * -1;
  var jsonCuentaOrdCompl3 ="\"importeCambio\":";
  this.importeCambio = req.params.importe * -1;
  this.descripcion = req.params.descripcion;
  if (req.params.tipoMov == "01"){
    this.importe = req.params.importe;
    this.importeCambio = req.params.importe;
    this.descripcion = "Transf-" + req.params.descripcion;
  }

  var jsonCuentaOrdCompl4 ="\"tipoCambio\":";
  this.tipoCambio = 1.0;
  var jsonCuentaOrdCompl5="\"fechaMov\":\"";
  this.fechaMov = (new Date()).toLocaleDateString();
  var jsonCuentaOrdCompl6="\"tipoMov\":\"";
  this.tipoMov = req.params.tipoMov;
  var jsonCuentaOrdCompl7="\"obsMov\":\"";
  var jsonCuentaOrdCompl8="\"}";

  var jsonCuentaFinal = jsonCuentaOrdCompl1 + jsonCuentaOrd + this.cuenta + urlComma + jsonCuentaOrdComp2 + this.importe +
                    urlCommaSinApost + jsonCuentaOrdCompl3 + this.importeCambio + urlCommaSinApost + jsonCuentaOrdCompl4 +
                    this.tipoCambio + urlCommaSinApost + jsonCuentaOrdCompl5 + this.fechaMov + urlComma +
                    jsonCuentaOrdCompl6 + this.tipoMov + urlComma + jsonCuentaOrdCompl7 + this.descripcion + jsonCuentaOrdCompl8;

  req.body = JSON.parse(jsonCuentaFinal);

  movbbvaMlabPost.post('',req.body,function(err, resM, body){
    res.send(body);
  });

})
